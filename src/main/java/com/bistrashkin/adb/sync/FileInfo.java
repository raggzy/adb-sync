package com.bistrashkin.adb.sync;

/**
 * Created by raggzy on 4/25/2016.
 */
public class FileInfo {
    public final String name;
    public final long size;

    public FileInfo(String name, long size) {
        this.name = name;
        this.size = size;
    }
}

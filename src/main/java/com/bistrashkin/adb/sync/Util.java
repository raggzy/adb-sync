package com.bistrashkin.adb.sync;

public class Util {
    public static String bashEscape(String arg) {
        return arg
                .replaceAll("[ ]", "\\\\ ")
                .replaceAll("[&]", "\\\\&")
                .replaceAll("[']", "\\\\'")
                .replaceAll("[\"]", "\\\\\"");
    }
}

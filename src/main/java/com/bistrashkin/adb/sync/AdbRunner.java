package com.bistrashkin.adb.sync;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AdbRunner {
    private static final String DEFAULT_ADB_PATH = "C:\\adb\\adb.exe";

    private final String adbPath;

    public AdbRunner(String adbPath) {
        this.adbPath = adbPath == null ? DEFAULT_ADB_PATH : adbPath;
    }

    public String push(String local, String remote) throws IOException, InterruptedException {
        return run("push", local, new String(remote.getBytes("utf8"), "cp1251"));
    }

    public String getState() throws IOException, InterruptedException {
        return run("get-state");
    }

    public String ls(String args, String remote) throws IOException, InterruptedException {
        return shell("ls " + args + " " + Util.bashEscape(remote));
    }

    public String rm(String args, String remote) throws IOException, InterruptedException {
        return shell("rm " + args + " " + Util.bashEscape(remote));
    }

    private String shell(String cmd) throws IOException, InterruptedException {
        return run("shell", new String(cmd.getBytes("utf8"), "cp1251"));
    }

    private String run(String... command) throws IOException, InterruptedException {
        List<String> pipeline = new ArrayList<>();
        pipeline.add(adbPath);
        Collections.addAll(pipeline, command);
        return new String(CmdRunner.run(pipeline).getBytes(), "utf8").trim();
    }
}

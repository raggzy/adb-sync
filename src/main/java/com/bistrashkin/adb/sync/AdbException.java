package com.bistrashkin.adb.sync;

/**
 * Created by raggzy on 5/6/2016.
 */
public class AdbException extends RuntimeException {
    private final String out;
    private final String err;

    public AdbException(String out, String err) {
        this.out = out;
        this.err = err;
    }

    public String getOut() {
        return out;
    }

    public String getErr() {
        return err;
    }
}

package com.bistrashkin.adb.sync;

import org.apache.commons.cli.*;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Hello world!
 */
public class Syncer {
    public static final Pattern DIRECTORY_PATTERN = Pattern.compile("^\\S+\\s+\\S+\\s+\\S+\\s+\\S+\\s+\\S+\\s+(.+)$");
    public static final Pattern FILE_PATTERN = Pattern.compile("^\\S+\\s+\\S+\\s+\\S+\\s+\\S+\\s+(\\S+)\\s+\\S+\\s+\\S+\\s+(.+)$");

    private final AdbRunner adb;

    public Syncer(AdbRunner adb) {
        this.adb = adb;
    }

    private Map<String, FileInfo> getRemoteFileInfo(String remoteDir, String lsOutput) {
        Map<String, FileInfo> result = new HashMap<>();

        List<String> lines = Arrays.stream(lsOutput.split("\n"))
                .map(String::trim)
                .filter(l -> !l.isEmpty()).collect(Collectors.toList());

        String base = null;
        for (String line : lines) {
            if (line.startsWith(remoteDir) && line.endsWith(":")) {
                base = line.replace(':', '/');
            } else {
                String linuxMode = line.substring(0, line.indexOf(" "));
                if (!linuxMode.startsWith("d")) {
                    FileInfo info = null;
                    Matcher fileMatcher = FILE_PATTERN.matcher(line);
                    if (fileMatcher.matches()) {
                        info = new FileInfo(fileMatcher.group(2), Long.valueOf(fileMatcher.group(1)));
                    }
                    if (info == null) {
                        System.out.println(String.format("==Strange line in ls output: [%s]. Skipping", line));
                    } else {
                        result.put(base + info.name, info);
                    }
                }
            }
        }
        return result;
    }

    private void getLocalFileInfoRecursive(String localDir, File current, Map<String, FileInfo> result) throws IOException {
        if (!localDir.equals(current.getCanonicalPath()) && current.isFile()) {
            result.put(current.getCanonicalPath(), new FileInfo(current.getName(), current.length()));
        }
        if (current.isDirectory()) {
            for (File f : current.listFiles()) {
                getLocalFileInfoRecursive(localDir, f, result);
            }
        }
    }

    private Map<String, FileInfo> getLocalFileInfo(String localDir) throws IOException {
        Map<String, FileInfo> result = new LinkedHashMap<>();
        getLocalFileInfoRecursive(localDir, new File(localDir), result);
        return result;
    }

    public void sync(String localDir, String remoteDir) throws IOException, InterruptedException {
        Map<String, FileInfo> localMap = getLocalFileInfo(localDir);
        Map<String, FileInfo> remoteMap = getRemoteFileInfo(remoteDir, adb.ls("-lR", remoteDir));
        System.out.println("Remote count: " + remoteMap.size() + ", local count: " + localMap.size());
        // remote over items
        for (String remote : remoteMap.keySet()) {
            String relative = remote.substring(remoteDir.length()).replace('/', File.separatorChar);
            String local = localDir + relative;
            if (!localMap.containsKey(local)) {
                System.out.println("Remote has over item:" + remote);
            }
        }
        // local sync
        for (String local : localMap.keySet()) {
            String relative = local.substring(localDir.length()).replace(File.separatorChar, '/');
            String remote = remoteDir + relative;
            FileInfo remoteInfo = remoteMap.get(remote);
            FileInfo localInfo = localMap.get(local);
            if (remoteInfo == null || remoteInfo.size != localInfo.size) {
                System.out.println(String.format("Syncing [%s] to [%s]", local, remote));
                try {
                    adb.push(local, remote);
                } catch (AdbException e) {
                    System.out.println("===Adb error while syncing");
                    System.out.println("===out: " + e.getOut());
                    System.out.println("===err: " + e.getErr());
                } catch (Exception e) {
                    System.out.println("===Error while syncing");
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws ParseException, IOException, InterruptedException {
        Options options = new Options();
        options.addOption("local", true, "local directory, absolute path, no slashes, contents you want to copy");
        options.addOption("remote", true, "remote directory, absolute path, no slashes, to where you want copy. Usually something like /storage/sdcard. But double-check");
        options.addOption("adbPath", false, "path to adb executable");
        CommandLineParser commandLineParser = new GnuParser();
        CommandLine commandLine = commandLineParser.parse(options, args);

        String localDir = commandLine.getOptionValue("local");
        String remoteDir = commandLine.getOptionValue("remote");
        String adbPath = commandLine.getOptionValue("adbPath");

        if (localDir == null || remoteDir == null) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("app", options);
            System.exit(-1);
        }

        AdbRunner adb = new AdbRunner(adbPath);
        String adbState = "";
        for (int i = 0; i < 10; i++) {
            adbState = adb.getState();
            if ("device".equals(adbState)) break;
            Thread.sleep(1000);
        }
        if (!"device".equals(adbState)) {
            System.out.println("Adb state is: " + adbState);
            System.out.println("Aborting");
            System.out.println("Check phone, drivers, etc. And when device ready try again");
            System.exit(-1);
        }

        Syncer syncer = new Syncer(adb);
        syncer.sync(localDir, remoteDir);
    }
}

package com.bistrashkin.adb.sync;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by raggzy on 4/25/2016.
 */
public class CmdRunner {
    public static String run(List<String> pipeline) throws IOException, InterruptedException {
        File err = File.createTempFile("cmd-run-err", "tmp");
        File out = File.createTempFile("cnd-run-out", "tmp");
        Process adb = new ProcessBuilder(pipeline)
                .redirectError(err)
                .redirectOutput(out)
                .start();
        int result = adb.waitFor();
        String error = IOUtils.toString(new FileInputStream(err));
        String output = IOUtils.toString(new FileInputStream(out));
        if (result != 0) {
            System.out.println(error);
            System.out.println(output);
            throw new AdbException(output, error);
        }
        return output;
    }

}
